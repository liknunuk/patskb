<div class="container-fluid">
    <div class="row">
        <div class="col">
            <h3><?= $data['title']; ?></h3>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th>Kelas</th>
                        <th>Nama Lengkap</th>
                        <th>Jawaban</th>
                        <th>Skor</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data['rekap'] as $rekap) : ?>
                        <tr>
                            <td><?= $rekap['kelas']; ?></td>
                            <td><?= strtoupper($rekap['namaLengkap']); ?></td>
                            <td><?= strtoupper($rekap['jawaban']); ?></td>
                            <td class="text-center"><?= $rekap['skor']; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>