<div class="container">
    <div class="row">
        <div class="col">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h1 class="display-4">KOREKSI PAT</h1>
                    <p class="lead">Sarana koreksi dan pengumpulan jawaban pilihan ganda PAT 2021 SKB Banjarnegara</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <p>Halaman ini difungsikan untuk mengirimkan hasil pengerjaan soal pilihan ganda Penilaian Akhir Tahun SKB Banjarnegara.</p>
            <p>Masukkan jawaban pilihan ganda secara berurutan pada kotak yang tersedia. Misal No. 1 = A, No. 2 = C, No. 3 = D, No. 4= B dan No. 5 = E, maka masukkan ACDBE, dan seterusnya</p>
        </div>
    </div>
    <div class="row bg-info py-3">
        <div class="col-md-4">
            <form action="<?= BASEURL; ?>Home/koreksi" method="post">
                <div class="form-group">
                    <label for="nama">Nama Siswa:</label>
                    <input type="text" class="form-control" name="nama" id="nama">
                </div>
                <div class="form-group">
                    <label for="">Kelas</label>
                    <select name="kelas" id="kelas" class="form-control">
                        <option value="07">Kelas VII / Kelas 7</option>
                        <option value="08">Kelas VIII / Kelas 8</option>
                        <option value="10">Kelas X / Kelas 10</option>
                        <option value="11">Kelas XI / Kelas 11</option>
                    </select>
                </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <label for="">Jawaban</label>
                <textarea name="jawaban" id="jawaban" rows="2" class="form-control" style="resize:none;" placeholder="Contoh: EABDABECABEABCABDCAABCADABBBEAAAEEEACDECDDDDC"></textarea>
            </div>
            <div class="form-group d-flex justify-content-end">
                <button type="submit" class="btn btn-success">Kirim Jawaban</button>
            </div>
        </div>
        </form>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>