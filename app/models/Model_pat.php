<?php
class Model_pat
{
    private $table = "skb_pat";
    // Kolom: namaLengkap , kelas, jawaban, skor, idx
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }


    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data)
    {
        $sql = "INSERT INTO $this->table SET namaLengkap=:nama , kelas=:kelas, jawaban=:jawaban, skor=:skor";
        $this->db->query($sql);
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('kelas', $data['kelas']);
        $this->db->bind('jawaban', $data['jawaban']);
        $this->db->bind('skor', $data['skor']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data)
    {
        $sql = "UPDATE $this->table SET namaLengkap=:nama , kelas=:kelas, jawaban=:jawaban, skor=:skor WHERE idx=:idx";
        $this->db->query($sql);
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('kelas', $data['kelas']);
        $this->db->bind('jawaban', $data['jawaban']);
        $this->db->bind('skor', $data['skor']);
        $this->db->bind('idx', $data['idx']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data)
    {
        $sql = "DELETE FROM $this->table WHERE idx=:idx";
        $this->db->query($sql);
        $this->db->bind('idx', $data['idx']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn = 1)
    {
        $row = ($pn - 1) * rows;
        $sql = "SELECT * FROM $this->table ORDER BY kelas,namaLengkap LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key)
    {
        $sql = "SELECT * FROM $this->table WHERE idx=:key ";
        $this->db->query($sql);
        $this->db->bind('key', $key);
        return $this->db->resultOne();
    }

    // CUSTOMIZED QUERY //

    public function rekap($kelas)
    {
        $sql = "SELECT * FROM skb_pat WHERE kelas=:kelas ORDER BY namaLengkap";
        $this->db->query($sql);
        $this->db->bind('kelas', $kelas);
        return $this->db->resultSet();
    }

    public function something($data)
    {
        // $sql = "";
        // $this->db->query($sql);
        // $this->db->bind('xxx', $data['xxx']);
        // $this->db->bind('xxx', $xxx);
        // return $this->db->resultSet();
    }
}

// QUERY TEMPLATE
/*
$sql = "";
$this->db->query($sql);
$this->db->bind();
$this->db->execute();
return $this->db->resultSet();
return $this->db->resultOne();
*/
