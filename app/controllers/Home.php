<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Home extends Controller
{
  // method default
  public function index()
  {
    $data = ['title' => "PAT 2021"];

    $this->view('template/header', $data);
    $this->view('home/index', $data);
    $this->view('template/footer');
  }

  public function koreksi()
  {
    $key = [
      '07' => 'cddbabccdaaddbdaaccaaacbbbbaacadabaadcbaabdba',
      '08' => 'dbcbdabdcbdadabdabcbbbddcaacacbddcaacaabcdbad',
      '10' => 'eabdabecabeabcabdcaabcadabbbeaaaeeeacdecddddc',
      '11' => 'eebbbabbcebbcabdaaaadeabaebeaaaceeaebacaccdee'
    ];
    $kelas = $_POST['kelas'];
    $kujaw = $key[$kelas];
    $udtg = 30;
    $skor = 0;
    // print_r($_POST);
    $len = strlen($_POST['jawaban']);
    for ($i = 0; $i < $len; $i++) {
      $nomor = $i + 1;
      if (strtolower($_POST['jawaban'][$i]) == $kujaw[$i]) {
        $skor += 1;
      }
    }
    $nilai = $skor + $udtg;
    $datapat = [
      'nama' => $_POST['nama'],
      'kelas' => $_POST['kelas'],
      'jawaban' => strtolower($_POST['jawaban']),
      'skor' => $nilai
    ];

    if ($this->model('Model_pat')->tambah($datapat) > 0) {

      $nilai = $skor + $udtg;
      echo "<hr/>";
      echo "Nilai perolehan pilihan ganda: " . $nilai;
      echo "<hr/>";
      echo "<a href='" . BASEURL . "'>Kembali</a>";
    }
  }

  public function rekap($kelas)
  {
    $data = [
      'title' => "Rekap Kelas {$kelas}",
      'rekap' => $this->model('Model_pat')->rekap($kelas)
    ];

    $this->view('template/header', $data);
    $this->view('home/rekap', $data);
    $this->view('template/footer');
  }
}
